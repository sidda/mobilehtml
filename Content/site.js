/*Dialogbox */
var inited = false;
function initDialog(clickedID, elementID) {
	//create transparent layer on body
	$('body').append('<div id="xj"></div>');
	var dlgHTML = '<div id="lj"><div id="optionsDlg" ></div></div>';
	$('#jqt').append(dlgHTML);
	$('#lj').prepend('<div id="dlgarrow" class="dlgarrow"></div>');
	//dont create dialog frame again
	inited = true;
}

function showDialog(clickedID, dialogContentID) {
	var eid = '#' + dialogContentID;
	var currentValue = $(eid).val();
	//if not already inited, init dialog now
	if(inited == 'undefined' || inited == false)
		initDialog(dialogContentID);
	//create content
	$('#lj #optionsDlg ul.rounded li a').remove();

	var itemList = $('<ul class="edgetoedge"></ul>');
	$(eid + ' option').each(function(index, item) {
		if(currentValue == item.innerHTML) {
			itemList.append("<li><a  href='#' rel='external' onclick=\"return hideDialog($(this).attr('value'),this.innerHTML,'" + dialogContentID + "')\" value='" + item.value + "' class='active'>" + item.innerHTML + "</a></li>");
		} else {
			itemList.append("<li><a  href='#' rel='external' onclick=\"return hideDialog($(this).attr('value'),this.innerHTML,'" + dialogContentID + "')\" value='" + item.value + "'>" + item.innerHTML + "</a></li>");
		}

	});
	//clear existing content and append new content to dialog
	$('#lj #optionsDlg').empty().append(itemList);
	$('#lj #optionsDlg ul.edgetoedge li a').tap(function() {
		//hideDialog($(this).attr('value'), $(this).html(), dialogContentID);
	});
	//position dialog
	$('#lj').adjust('#' + clickedID);
	//show dialog now
	//$("#xj").show();
	$("#lj").show();
	//$("#lj").center();
	//disable click by returning false;
	return false;

}

function hideDialog(value, text, dialogContentID) {

	$('#' + dialogContentID).val(value);
	$('#link-' + dialogContentID + " b.text").text(value);
	$("#xj").hide();
	$("#lj").hide();
	return false;
}

jQuery.fn.center = function() {
	this.css("position", "absolute");
	this.css("top", (($(window).height() - this.outerHeight()) / 2) + $(window).scrollTop() + "px");
	this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
	return this;
}
jQuery.fn.adjust = function(sourceID) {
	var dpos = $(sourceID).position();
	console.log(dpos);
	this.css("position", "absolute");
	this.css("top", (dpos.top + 20) + "px");
	this.css("left", (dpos.left + 30) + "px");
	return this;
}